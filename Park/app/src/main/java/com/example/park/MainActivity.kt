package com.example.park

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    private val myParking: ArrayList<Slot> = ArrayList<Slot>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val slot1: Button = findViewById(R.id.slot_btn1)
        val slot2: Button = findViewById(R.id.slot_btn2)
        val slot3: Button = findViewById(R.id.slot_btn3)

        for (i in 0..2) {
            myParking.add(i, Slot("", "", ""))
        }

        findViewById<Button>(R.id.slot_btn1).setOnClickListener {
            click()

        }

        findViewById<Button>(R.id.slot_btn2).setOnClickListener {
            click2()
        }

        findViewById<Button>(R.id.slot_btn3).setOnClickListener {
            click3()
        }
    }

    fun click() {
        val slot1 = findViewById<Button>(R.id.slot_btn1)
        val slot2: Button = findViewById(R.id.slot_btn2)
        val slot3: Button = findViewById(R.id.slot_btn3)

        val register = findViewById<EditText>(R.id.register)
        val driver = findViewById<EditText>(R.id.driver)
        val band = findViewById<EditText>(R.id.band)

        slot1.setBackgroundColor(Color.RED);
        slot2.setBackgroundResource(android.R.drawable.btn_default);
        slot3.setBackgroundResource(android.R.drawable.btn_default);

        register.setText(myParking.get(0).register).toString()
        driver.setText(myParking.get(0).driver).toString()
        band.setText(myParking.get(0).band).toString()

        var id = register.text
        var brand = driver.text
        var name = band.text
        findViewById<Button>(R.id.btn_submit).setOnClickListener {
            myParking.add(0, Slot(id.toString(), brand.toString(), name.toString()))
            slot1.setText(myParking.get(0).register).toString()
            Log.d("rgister", myParking.get(0).register).toString()

            register.setText("")
            driver.setText("")
            band.setText("")

        }


    }

    fun click2() {
        val slot1 = findViewById<Button>(R.id.slot_btn1)
        val slot2: Button = findViewById(R.id.slot_btn2)
        val slot3: Button = findViewById(R.id.slot_btn3)

        val register = findViewById<EditText>(R.id.register)
        val driver = findViewById<EditText>(R.id.driver)
        val band = findViewById<EditText>(R.id.band)

        slot1.setBackgroundResource(android.R.drawable.btn_default);
        slot2.setBackgroundColor(Color.RED);
        slot3.setBackgroundResource(android.R.drawable.btn_default);

        register.setText(myParking.get(1).register).toString()
        driver.setText(myParking.get(1).driver).toString()
        band.setText(myParking.get(1).band).toString()

        var id = register.text
        var brand = driver.text
        var name = band.text
        findViewById<Button>(R.id.btn_submit).setOnClickListener {
            myParking.add(1, Slot(id.toString(), brand.toString(), name.toString()))
            slot2.setText(myParking.get(1).register).toString()

            register.setText("")
            driver.setText("")
            band.setText("")
        }

    }

    fun click3() {
        val slot1 = findViewById<Button>(R.id.slot_btn1)
        val slot2: Button = findViewById(R.id.slot_btn2)
        val slot3: Button = findViewById(R.id.slot_btn3)

        val register = findViewById<EditText>(R.id.register)
        val driver = findViewById<EditText>(R.id.driver)
        val band = findViewById<EditText>(R.id.band)

        slot1.setBackgroundResource(android.R.drawable.btn_default);
        slot2.setBackgroundResource(android.R.drawable.btn_default);
        slot3.setBackgroundColor(Color.RED);

        register.setText(myParking.get(2).register).toString()
        driver.setText(myParking.get(2).driver).toString()
        band.setText(myParking.get(2).band).toString()

        var id = register.text
        var brand = driver.text
        var name = band.text
        findViewById<Button>(R.id.btn_submit).setOnClickListener {
            myParking.add(2, Slot(id.toString(), brand.toString(), name.toString()))
            slot3.setText(myParking.get(2).register).toString()

            register.setText("")
            driver.setText("")
            band.setText("")
        }

    }


}
