package com.betch59161072

import android.media.Image
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_my_app.*
import java.util.*

class MyApp : AppCompatActivity() {

   // var diceImage:ImageView? = null
   lateinit var diceImage:ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_app)

        diceImage = findViewById(R.id.dice_image)

        val rollButton: Button = findViewById(R.id.roll_button)
        rollButton.setOnClickListener { rollDice() }

        val resetButton: Button = findViewById(R.id.reset_button)
        resetButton.setOnClickListener { resetDice() }
    }

    private fun rollDice() {
        val randomInt = Random().nextInt(6) + 1


        val drawableResource = when (randomInt) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
        //diceImage?.setImageResource(drawableResource)
        diceImage.setImageResource(drawableResource)
//        Toast.makeText(this, "Button click", Toast.LENGTH_SHORT).show()
    }

    private fun resetDice() {
        val drawableResource = R.drawable.empty_dice
        diceImage.setImageResource(drawableResource)
    }
}
